import SwiftUI

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ContentView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct ContentView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack(spacing: 8.0) {
            
            // MARK: -∂ TopNavigationBar
            TopNavigationBarView()
                .padding(.top, 60)
                .padding(.leading, 30)
                .padding(.trailing, 30)
            
            // MARK: -∂ DashboardView
            DashboardView()
                .padding(.leading, 30)
                .padding(.trailing, 30)
            
            // MARK: -∂ GraphBarView
            GraphBarView()
                .padding(.leading, 30)
                .padding(.trailing, 30)
            
            TabBarView()
                .frame(height: 100)
                .offset(y: 105)
            //                .padding(.bottom, 40)
            
            Spacer()// Vertically spaced
            
        }//||END__PARENT-VSTACK||
        .environmentObject(DashboardManager())
        .background(ColorConstants.backgroundLinear)
        .shadow(color: Color.black.opacity(0.25), radius: 10, x: 15, y: 12)
        .edgesIgnoringSafeArea(.all)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

// MARK: Helper Function
func iAmHere(myStr: String) -> some View {
    return Text("\(myStr)")
        .font(.system(size: 22))
        .foregroundColor(.white)
        .bold()
        .background(Color.black)
}
