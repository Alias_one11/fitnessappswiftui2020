//
//  FtnessApp_SwiftUISept15App.swift
//  FtnessApp_SwiftUISept15
//
//  Created by Jose Martinez on 9/15/20.
//

import SwiftUI

@main
struct FtnessApp_SwiftUISept15App: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
