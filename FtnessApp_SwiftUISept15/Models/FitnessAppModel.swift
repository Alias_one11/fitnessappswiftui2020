import SwiftUI

import SwiftUI

struct FitnessAppModel {
    
    // MARK: -∂ DashboardMenu
    struct DashboardMenu: Identifiable {
        // MARK: - ©Global-PROPERTIES
        /*..............................*/
        let id = UUID().uuidString
        let title, name, unit, currentValue: String
        var selected: Bool = false
        /*..............................*/
    }
    /*...................................................*/
    
    // MARK: -∂ BPM
    struct BPM: Identifiable {
        // MARK: - ©Global-PROPERTIES
        /*..............................*/
        let id = UUID().uuidString
        let value: CGFloat
        let time: String
        var selected: Bool = false
        /*..............................*/
    }
    /*...................................................*/
    
    // MARK: -∂ HeartRateMenu
    struct HeartRateMenu: Identifiable {
        // MARK: - ©Global-PROPERTIES
        /*..............................*/
        let id: Int
        let name: String
        var currentValue: Int
        var selected: Bool = false
        /*..............................*/
    }
    /*...................................................*/
    
}///:|_FitnessAppModel_|
