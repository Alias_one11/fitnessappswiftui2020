import SwiftUI

struct NeomorphicRectangleInverted: ViewModifier {
    
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let topBlur, bottomBlur,
        cornerRadius, topOffset,
        bottomOffset: CGFloat
    /*..............................*/
    func body(content: Content) -> some View {
        //__________
        content
            // - ©Layers a secondary view in front of this view.
            .overlay(
                //__________
                // - ©Gives the overlay a neomorphic look
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(Color.black, lineWidth: 2)
                    .cornerRadius(cornerRadius)
                    .blur(radius: topBlur)
                    .offset(x: topOffset, y: topOffset)
                    .mask(RoundedRectangle(cornerRadius: cornerRadius))
            )
            .overlay(
                //__________
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(ColorConstants.whiteClearWithStopLinear,
                            lineWidth: 2)
                    .blur(radius: bottomBlur)
                    .offset(x: bottomOffset, y: bottomOffset)
                    .mask(RoundedRectangle(cornerRadius: cornerRadius))
            )
    }
    /*...................................................*/
    
}///:|_NeomorphicRectangleInverted_|

extension View {
    //__________
    func invertedRectangleNeomorphify(
        topBlur: CGFloat = 3, bottomBlur: CGFloat = 6,
        cornerRadius: CGFloat = 10, topOffset: CGFloat = 3,
        bottomOffset: CGFloat = -2) -> some View {
        //__________
        modifier(
            NeomorphicRectangleInverted(
                topBlur: topBlur,
                bottomBlur: bottomBlur,
                cornerRadius: cornerRadius,
                topOffset: topOffset,
                bottomOffset: bottomOffset)
        )
    }
    /*...................................................*/
    
    func invertedRectangleMiddleNeomorphify() -> some View {
        //__________
        modifier(NeomorphicRectangleInvertedTopBottom())
    }
    
}
