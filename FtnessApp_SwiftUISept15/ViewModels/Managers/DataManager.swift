import SwiftUI

class DataManager: ObservableObject {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @Published var bpmsValues = bpms
    var lastSelectedBPM: Int = -1
    /*..............................*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func selectDataWith(bpm: FitnessAppModel.BPM) {
        //__________
        if let foundIndex = bpmsValues.firstIndex(where: { $0.id == bpm.id }) {
            //__________
            bpmsValues[foundIndex].selected.toggle()
            
            if (foundIndex == lastSelectedBPM) {
                lastSelectedBPM = -1
                //__________
            } else if (lastSelectedBPM != -1) {
                bpmsValues[lastSelectedBPM].selected.toggle()
                //__________
            }
            //__________
            lastSelectedBPM = foundIndex
        }
    }
    /*...................................................*/
    
}///:|_DataManager_|
