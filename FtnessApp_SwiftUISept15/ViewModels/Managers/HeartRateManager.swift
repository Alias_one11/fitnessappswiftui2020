import SwiftUI

class HeartRateManager: ObservableObject {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @Published var menus = heartRateMenu
    @Published var lastSelectedMenuIndex: Int = 0
    /*..............................*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func selectMenuWith(index: Int) {
        //__________
        menus[index].selected = true
        
        if index != lastSelectedMenuIndex {
            //__________
            menus[lastSelectedMenuIndex].selected = false
            lastSelectedMenuIndex = index
        }
    }
    /*...................................................*/
    
}///:|_HeartRateManager_|
