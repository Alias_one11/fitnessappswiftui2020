import SwiftUI

class DashboardManager: ObservableObject {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    typealias MenuModel = [FitnessAppModel.DashboardMenu]
    @Published var dashboardMenus: MenuModel = menus
    @Published var selectedMenuIndex: Int = 0
    /*..............................*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func selectMenuWith(index: Int) {
        //__________
        dashboardMenus[index].selected  = true
        
        if index != selectedMenuIndex {
            //__________
            dashboardMenus[selectedMenuIndex].selected = false
            selectedMenuIndex = index
        }
    }
    /*...................................................*/
}
