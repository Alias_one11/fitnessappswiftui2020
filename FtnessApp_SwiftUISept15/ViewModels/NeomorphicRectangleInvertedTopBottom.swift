import SwiftUI

struct NeomorphicRectangleInvertedTopBottom: ViewModifier {
    
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    /*..............................*/
    func body(content: Content) -> some View {
        //__________
        content
            // - ©Layers a secondary view in front of this view.
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color.black, lineWidth: 2)
                    .blur(radius: 4)
                    .offset(x: 0, y: 3)
                    .mask(RoundedRectangle(cornerRadius: 20))
            )
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(ColorConstants.whiteClearWithStopTopMiddleToBottomMiddleLinear,
                            lineWidth: 2)
                    .blur(radius: 4)
                    .offset(x: 0, y: -3)
                    .mask(RoundedRectangle(cornerRadius: 20))
            )
    }
    /*...................................................*/
    
}///:|_NeomorphicRectangleInvert
