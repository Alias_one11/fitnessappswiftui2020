import SwiftUI

struct TopNavigationBarView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var dashboardManager: DashboardManager
    @State var showHeartRateView: Bool = false
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        //__________
        let menuIndex = dashboardManager
            .dashboardMenus[dashboardManager.selectedMenuIndex]
        
        //||_PARENT__=>VSTACK||
        //............................./
        HStack {
            //__________
            
            VStack(alignment: .leading) {
                //__________
                // MARK: -∂ SFSymbol Rectangle leftthird
                Image(systemName: "rectangle.leftthird.inset.fill")
                    .resizable()
                    .frame(width: 20, height: 10)
                    .shadow(color: Color.black, radius: 8, x: 15, y: 8)
                    .foregroundColor(ColorConstants.textCircleSecendary)

                // MARK: -∂ Title
                Text(menuIndex.title)
                    .bold()
                    .shadow(color: Color.black.opacity(0.99), radius: 8, x: 15, y: 8)
                    .foregroundColor(.white)
            }///:|_VStack_|
            /*©---------------------------------©*/
            
            Spacer()// Horizontally spaced
            
            // MARK: - Child-Button
            /**.................................*/
            Button(action: {
                //__________
                showHeartRateView.toggle()// from false to true
                
            }, label: {
                //__________
                // MARK: -∂ FourCircleView
                FourCircleView()
                    .shadow(color: Color.black.opacity(0.85),
                            radius: 8, x: 12, y: 8)
            })
            // MARK: - sheet
            //--|............................................
            .sheet(isPresented: $showHeartRateView, content: {
                //__________
                HeartRateSettingsView()
                
            })
            //--|............................................
            /**.................................*/
            
        }//||END__PARENT-HSTACK||
        .shadow(color: Color.black.opacity(0.19), radius: 10)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
