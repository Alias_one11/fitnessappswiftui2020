import SwiftUI

struct FourCircleView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {
            //__________
            // MARK: -∂ Neoform butto
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.clear)
                .invertedRectangleNeomorphify(bottomOffset: -1)
                .clipShape(RoundedRectangle(cornerRadius: 10))
            
            HStack(spacing: 14) {
                //__________
                // MARK: - Child-Circles in FourCircleView
                dotsComponents()// Left dots
                dotsComponents()// Right dots
                /**.................................*/
                
            }///:|_HStack_|
            
            /*©---------------------------------©*/
            
        }//||END__PARENT-VSTACK||
        .frame(width: 60, height: 60)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
    fileprivate func dotsComponents() -> some View {
        return //__________
            // MARK: - Child-Circles in FourCircleView
            /**.................................*/
            VStack(spacing: 14) {
                //__________
                Circle()
                    .frame(width: 5, height: 5)
                
                Circle()
                    .frame(width: 5, height: 5)
            }
            .foregroundColor(.white)
    }
}// END: [STRUCT]

