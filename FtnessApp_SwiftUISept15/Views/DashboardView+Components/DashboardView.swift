import SwiftUI

struct DashboardView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var dashboardManager: DashboardManager
    var selected: Bool = false
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        //__________
        let menuIndex = dashboardManager
            .dashboardMenus[dashboardManager.selectedMenuIndex]
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {
            
            // MARK: - Child-HexagonRounded Circle in the middle
            /**.................................*/
            HexagonRounded()
                .fill(ColorConstants.hexagonLinear)
                .frame(height: 340)
                .shadow(color: Color.black.opacity(0.19), radius: 10)

            
            Circle()
                .fill(ColorConstants.circle)
                .frame(width: 180, height: 180)
                .shadow(color: Color.white.opacity(0.15),
                        radius: 20, x: -10, y: -10)
                .shadow(color: Color.black.opacity(0.4),
                        radius: 20, x: 10, y: 10)
            /**.................................*/
            
            VStack {
                //__________
                // MARK: -∂ currentValue
                Text(menuIndex.currentValue)
                    .font(.system(size: 47))
                    .bold()
                    .foregroundColor(ColorConstants.textCirclePrimary)
                    .shadow(color: Color.black.opacity(0.85), radius: 8, x: 12, y: 10)
                
                // MARK: -∂ unit
                Text(menuIndex.unit.uppercased())
                    .bold()
                    .foregroundColor(ColorConstants.textCircleSecendary)
                    .shadow(color: Color.black.opacity(0.85), radius: 8, x: 12, y: 8)
                
            }///:|_VStack_|
            
            // MARK: - Child-HStack
            /**.................................*/
            HStack(spacing: 30) {
                //__________
                Image(systemName: "ellipsis")
                    .rotationEffect(Angle.degrees(90))
                    .fixedSize()
                    .foregroundColor(ColorConstants.unselectedforeground)
                    .frame(width: 20, height: 60)
                
                HStack(spacing: 140) {
                    //__________
                    // MARK: -∂ sfSymbolLeadingComponent heart, lighting
                    sfSymbolLeadingComponent(sfSymbolIndex1: 0, sfSymbolIndex2: 1)
                        .foregroundColor(ColorConstants.unselectedforeground)
                        .shadow(color: selected ? ColorConstants.selectedColor : ColorConstants.unselectedforeground.opacity(0.95), radius: 10)
                    
                    // MARK: -∂ sfSymbolTrailingComponent rain, temperature
                    sfSymbolTrailingComponent(sfSymbolIndex1: 2, sfSymbolIndex2: 3)
                        .foregroundColor(ColorConstants.unselectedforeground)
                        .shadow(color: selected ? ColorConstants.selectedColor : ColorConstants.unselectedforeground.opacity(0.95), radius: 10)
                    
                }///:|_HStack_|
               
                /*©---------------------------------©*/
                Text("Analyze")
                    .bold()
                    .rotationEffect(Angle.degrees(90))
                    .fixedSize()
                    .foregroundColor(ColorConstants.unselectedforeground)
                    .frame(width: 20, height: 60)
                    .shadow(color: Color.black, radius: 8, x: -8, y: 0)
            }
            
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        
        //............................./
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
