import SwiftUI

struct sfSymbolTrailingComponent: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var dashboardManager: DashboardManager
    var sfSymbolIndex1: Int
    var sfSymbolIndex2: Int
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        //__________
        let sfIndex1 = dashboardManager.dashboardMenus[sfSymbolIndex1]
        let sfIndex2 = dashboardManager.dashboardMenus[sfSymbolIndex2]
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack(spacing: 140) {
            //__________
            Image(systemName: sfIndex1.name)
                .font(.system(size: 30))
                .foregroundColor(
                    dashboardManager.selectedMenuIndex == 2 ?
                        ColorConstants.selectedColor :
                        ColorConstants.unselectedforeground)
                // MARK: - onTapGesture
                //@`````````````````````````````````````````````
                .onTapGesture(perform: {
                    //__________
                    dashboardManager.selectMenuWith(index: 2)
                })
            //@-`````````````````````````````````````````````
            
            Image(systemName: sfIndex2.name)
                .font(.system(size: 30))
                .foregroundColor(
                    dashboardManager.selectedMenuIndex == 3 ?
                        ColorConstants.selectedColor :
                        ColorConstants.unselectedforeground)
                // MARK: - onTapGesture
                //@`````````````````````````````````````````````
                .onTapGesture(perform: {
                    //__________
                    dashboardManager.selectMenuWith(index: 3)
                })
            //@-`````````````````````````````````````````````
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

