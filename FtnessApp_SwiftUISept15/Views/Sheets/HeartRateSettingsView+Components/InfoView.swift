import SwiftUI

struct InfoView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var status: Bool = true
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack(alignment: .leading) {
            
            //__________
            // MARK: - Child-HStack
            /**.................................*/
            HStack {
                
                //__________
                // MARK: -∂ BackButtonView action .top, .leading
                Button(action: {
                    //__________
                    // - ©Will dismiss the View/Sheet when the button is pressed
                    presentationMode.wrappedValue.dismiss()
                    
                }, label: {
                    //__________
                    BackButtonView()
                })//:|_Button_|
                /*...................................................*/
                
                VStack(alignment: .leading) {
                    //__________
                    // MARK: -∂ Back to
                    Text("Back to")
                        .foregroundColor(ColorConstants.textCircleSecendary)
                        .bold()
                    // MARK: -∂ settings
                    Text("settings")
                        .foregroundColor(.white)
                        .bold()
                }
                .shadow(color: Color.black.opacity(0.75), radius: 10, x: 5, y: 8)
                /*©---------------------------------©*/
                
                Spacer()// Horizontally spaced
                
                
            }///:|_HStack_|
            
            /**.................................*/
            
            Text("Heart\nLimit")
                .font(.system(size: 50))
                .bold()
                .foregroundColor(.white)
                .shadow(color: Color.black, radius: 10, x: 15, y: 12)
                .padding(.top, 50)
            
            Text("Set a heart rate limit to control the time")
                .font(.system(size: 14))
                .bold()
                .foregroundColor(ColorConstants.textCircleSecendary)
                .shadow(color: Color.black.opacity(0.95), radius: 10, x: 5, y: 8)
            /*...................................................*/
            
            // MARK: -∂ HeartRateOptionView
            HeartRateOptionView()// Vertically stacked
            
            Spacer()// Stacked Vertically
            
            // MARK: -∂ Toggle
            Toggle(isOn: $status, label: {
                Text("Notification")
                    .font(.callout)
                    .foregroundColor(ColorConstants.textCircleSecendary)
                    .bold()
                    .shadow(color: Color.black.opacity(0.45), radius: 5, x: 12, y: 10)
            })
            .toggleStyle(CustomToggleStyleView())
            .animation(.linear(duration: 0.1))
            .shadow(color: Color.black.opacity(0.35), radius: 5, x: 10, y: 8)
            /*©---------------------------------©*/
            
        }//||END__PARENT-VSTACK||
        .padding(.leading, 30)
        .padding(.bottom, 40)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
}// END: [STRUCT]
