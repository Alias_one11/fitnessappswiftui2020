import SwiftUI

struct VerticalSliderComponent: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @State var progress: CGFloat = -0.1
    let minProgress: CGFloat = 0.07
    let maxProgress: CGFloat = 1.0
    let step: CGFloat = 0.05// Bars of sliders thickness
    @Binding var bpm: Int
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        GeometryReader { geo in
            
            ZStack {
                //__________
                SliderShape(progress: progress, step: step)
                    .fill(ColorConstants.whiteToDarkGrayLinear)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .shadow(color: Color.black.opacity(0.75), radius: 10, x: -5, y: -5)
                    // MARK: - gesture
                    //--|............................................
                    .gesture(
                        DragGesture(minimumDistance: 5)
                            .onChanged({ value in
                                //__________
                                /// - ©Progresses vertically(y)
                                let nextProgress = 1 - (value.location.y / geo.size.height)
                                updateBPMWith(nextProgress: nextProgress)
                            })
                    )
                    .onAppear(perform: {
                        //__________
                        updateInitialProgress()
                    })
                //--|............................................
                
                ThumbView(width: 18, spacing: 6)
                    .offset(y: calculateThumbPosition(height: geo.size.height))
                    .shadow(color: Color.black.opacity(0.35), radius: 10, x: 5, y: 5)
                /*...................................................*/
            }///:|_ZStack_|
            /*©---------------------------------©*/
            .shadow(color: Color.black.opacity(0.35), radius: 10, x: 5, y: 5)

        }//||END__PARENT-GeometryReader||
        .padding(.top, 75)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func updateInitialProgress() {
        //__________
        /// - ©Progresses the slider
        progress = CGFloat(bpm) / 250
    }
    /*©---------------------------------©*/
    
    func updateBPMWith(nextProgress: CGFloat) {
        //__________
        if (nextProgress >= minProgress &&
                nextProgress <= maxProgress) {
            //__________
            let stepProgress = round(nextProgress / step) * step
            progress = max(minProgress, stepProgress)
            bpm = Int(progress * 250)
        }
    }
    /*©---------------------------------©*/
    
    func calculateThumbPosition(height: CGFloat) -> CGFloat {
        //__________
        var position: CGFloat
        let progressFromMiddle = 0.5 - progress
        position = (progressFromMiddle * height) + 15
        
        return position
    }
    /*©---------------------------------©*/
    
    func calculateThumbPosition(height: CGFloat, forProgress: CGFloat) -> CGFloat {
        //__________
        var position: CGFloat
        let progressFromMiddle = 0.5 - progress
        position = (progressFromMiddle * height) + 15
        
        return position
    }
    
    /*...................................................*/
    
}// END: [STRUCT]
