import SwiftUI

struct ControlView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
//    @Binding var heartRate: Int
    @EnvironmentObject var manager: HeartRateManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        let menusCurrentValue = $manager.menus[manager.lastSelectedMenuIndex].currentValue
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            //__________
            ZStack {
                
                Circle()
                    .fill(ColorConstants.hexagonLinearInverted)
                    .frame(width: 50, height: 50)
                
                Text("i")
                    .font(.title)
                    .bold()
                    .foregroundColor(.white)
                    .shadow(color: Color.black, radius: 8, x: 12, y: 8)
                
            }//||END__PARENT-VSTACK||
            .shadow(color: Color.black.opacity(0.35), radius: 10, x: 5, y: 5)
            .offset(x: 90)
            /*©---------------------------------©*/
            
            Spacer()
        }///:|_VStack_|
        .padding(.top, 0)
        //............................./
        
        // MARK: -∂ VerticalSliderComponent
        VerticalSliderComponent(bpm: menusCurrentValue)
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

