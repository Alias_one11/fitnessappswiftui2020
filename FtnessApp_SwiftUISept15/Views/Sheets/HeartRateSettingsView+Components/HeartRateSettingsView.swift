import SwiftUI

import SwiftUI

// MARK: - Preview
struct HeartRateSettingsView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        HeartRateSettingsView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct HeartRateSettingsView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        HStack(spacing: 0) {
            
            // MARK: -∂ InfoView
            InfoView()
            
            // MARK: -∂ ControlView
            ControlView()
                .frame(width: 100)
            
        }//||END__PARENT-VSTACK||
        .padding(.top, 50)
        .shadow(color: Color.black.opacity(0.35), radius: 10, x: 15, y: 12)
        .background(ColorConstants.heartRateBackground)
        .edgesIgnoringSafeArea(.all)
        .environmentObject(HeartRateManager())
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
