import SwiftUI

struct CustomToggleStyleView: ToggleStyle {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    /*..............................*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func makeBody(configuration: Configuration) -> some View {
        //__________
        return HStack {
            //__________
            ZStack {
                
                RoundedRectangle(cornerRadius: 20)
                    .fill(Color.clear)
                    .frame(width: 60, height: 40)
                    .invertedRectangleMiddleNeomorphify()
                /*...................................................*/
                
                Group {
                    Circle()
                        .fill(configuration.isOn ? ColorConstants.selectedColor :
                                ColorConstants.toggleUnselected)
                        .frame(width: 25, height: 25)
                    
                    // MARK: -∂ ThumbView
                    ThumbView(width: 9, spacing: 4)
                        .foregroundColor(ColorConstants.unselectedforeground)
                }
                /// - ©Toggles our CustomToggleStyleView
                .offset(x: configuration.isOn ? 13 : -13)
                /*...................................................*/
                
            }
            .padding(.trailing, 10)
            .shadow(color: Color.black.opacity(0.55), radius: 10, x: 5, y: 8)
            /*©---------------------------------©*/
            
            // MARK: -∂ typealias Configuration = ToggleStyleConfiguration
            configuration.label
            
        }//||END__PARENT-HSTACK||
        .shadow(color: Color.black.opacity(0.55), radius: 10, x: 5, y: 8)
        // MARK: - onTapGesture Toggles notification on tapped
        //--|............................................
        .onTapGesture(perform: {
            //__________
            withAnimation {
                //__________
                configuration.isOn.toggle()
            }
        })///:|_onTapGesture_|
        //--|............................................
    }
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
