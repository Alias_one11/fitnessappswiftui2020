import SwiftUI

struct ThumbView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let width: CGFloat
    let spacing: CGFloat
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack(spacing: spacing) {
            
            Rectangle()
                .frame(width: width, height: 2)
            
            Rectangle()
                .frame(width: width, height: 2)
            
        }//||END__PARENT-VSTACK||
        .foregroundColor(.black)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
