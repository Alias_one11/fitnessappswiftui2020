import SwiftUI

struct HeartRateOptionView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var manager: HeartRateManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        let menuIndex = manager.menus[manager.lastSelectedMenuIndex].currentValue
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            //__________
            ZStack {
                
                RoundedRectangle(cornerRadius: 30)
                    .fill(Color.clear)
                    // MARK: - Adds a neomorphic look to the rectangle
                    //--|............................................
                    .invertedRectangleNeomorphify(topBlur: 6, bottomBlur: 8, cornerRadius: 40)
                    //--|............................................
                    .frame(width: 170, height: 170)
                    .padding(.top, 16)
                /*...................................................*/
                
                // MARK: -∂ BPM Toggled changed state
                VStack {
                    Text("\(menuIndex)ᵇᵖᵐ")
                        .font(.system(size: 43))
                        .bold()
                        .foregroundColor(.white)
                    
                    Text("BPM")
                        .bold()
                        .foregroundColor(ColorConstants.textCircleSecendary)
                    
                }///:|_VStack_|
                .shadow(color: Color.black.opacity(0.95), radius: 10, x: 15, y: 12)
                /*©---------------------------------©*/
                
            }///:|_ZStack_|
            
            // MARK: -∂ OptionMenuView Resting, Gym
            /**.................................*/
            HStack(spacing: 30) {
                //__________
                ForEach(manager.menus) { menu in
                    //__________
                    OptionMenuView(menu: menu)
                        // MARK: - onTapGesture
                        //--|............................................
                        .onTapGesture(perform: {
                            //__________
                            manager.selectMenuWith(index: menu.id)
                        })
                        //--|............................................
                }
                .padding(.top, 10)
                
            }///:|_HStack_|
            .onAppear(perform: {
                //__________
                manager.selectMenuWith(index: 1)
            })
            
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        .shadow(color: Color.black.opacity(0.09), radius: 10, x: 15, y: 8)
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]



/*...................................................*/


