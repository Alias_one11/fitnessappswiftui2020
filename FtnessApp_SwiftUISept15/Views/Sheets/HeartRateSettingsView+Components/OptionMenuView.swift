import SwiftUI

struct OptionMenuView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let menu: FitnessAppModel.HeartRateMenu
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack(spacing: 4) {
            
            /// - ©Resting, Gym selected, not selected color toggle
            Text(menu.name)
                .foregroundColor(menu.selected ? Color.white :
                                    ColorConstants.textCircleSecendary)
                .bold()
            
            /// - ©Underline glowing line. Under Resting, Gym
            Rectangle()
                .fill(ColorConstants.selectedColor)
                .frame(width: 40, height: 2)
                .shadow(color: ColorConstants.selectedColor, radius: 8)
                .opacity(menu.selected ? 1.0 : 0)
            
        }//||END__PARENT-VSTACK||
        .shadow(color: Color.black.opacity(0.95), radius: 10, x: 8, y: 8)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
