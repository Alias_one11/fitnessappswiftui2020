import SwiftUI

struct GraphCellComponent: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let bpm: FitnessAppModel.BPM
    let barHeight: CGFloat
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            // MARK: -∂ BPM values
            Text(String.init(format: "%.0f", bpm.value))
                .font(.system(size: 15))
                .foregroundColor(Color.white)
                .bold()
                .shadow(color: Color.black.opacity(0.75), radius: 10, x: 12, y: 8)
            /*...................................................*/
            // MARK: -∂ Graph bars
            RoundedRectangle(cornerRadius: 8)
                .fill(bpm.selected ? ColorConstants.selectedColor :
                        ColorConstants.graphUnselected)
                .frame(height: barHeight)
                .shadow(color: bpm.selected ? ColorConstants.selectedColor : Color.black.opacity(0.19), radius: 10)
            /*...................................................*/
            // MARK: -∂ BPM time
            Text(bpm.time)
                .font(.system(size: 13))
                .foregroundColor(ColorConstants.textCircleSecendary)
                .bold()
                .shadow(color: Color.black.opacity(0.85), radius: 10, x: 12, y: 8)
            /*...................................................*/
        }//||END__PARENT-VSTACK||
        .shadow(color: Color.black.opacity(0.75), radius: 10, x: 5, y: 8)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
