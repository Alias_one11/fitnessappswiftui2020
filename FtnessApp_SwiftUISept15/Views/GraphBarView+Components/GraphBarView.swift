import SwiftUI

struct GraphBarView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @ObservedObject var manager = DataManager()
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            HStack {
                //__________
                Image(systemName: "calendar.badge.clock")

                Text("Average")
                    .bold()
                    .foregroundColor(.white)
                
                Spacer()// Horizontally spaced
                
            }///:|_HStack_|
            .foregroundColor(ColorConstants.textCircleSecendary)
            .shadow(color: Color.black.opacity(0.95), radius: 8, x: 12, y: 8)
            /*©---------------------------------©*/
            
            // MARK: - Child-HStack
            /**.................................*/
            HStack(alignment: .bottom, spacing: 14) {
                //__________
                ForEach(manager.bpmsValues) { bpm in
                    //__________
                    // MARK: -∂ GraphCellComponent
                    GraphCellComponent(
                        bpm: bpm, barHeight: getRealitiveHeightWith(value: bpm.value)
                    )
                        // MARK: - onTapGesture
                        //@`````````````````````````````````````````````
                        .onTapGesture(perform: {
                            //__________
                            withAnimation {
                                //__________
                                manager.selectDataWith(bpm: bpm)
                            }
                        })
                    //@-`````````````````````````````````````````````
                }
                
                // MARK: - Child-HStack
                /**.................................*/
                HStack {
                    //__________
                    Text("AVG BPM: ")
                        .bold()
                        .foregroundColor(ColorConstants.textCircleSecendary)
                    
                    Text("Per Hours")
                        .bold()
                        .foregroundColor(.white)
                }
                .font(.system(size: 17))
                .fixedSize()
                .frame(width: 20, height: 180)
                .rotationEffect(Angle.degrees(-90))
                .shadow(color: Color.black, radius: 8, x: 8, y: 0)
                .offset(y: -16)
                /**.................................*/
                
            }//:|_HStack_|
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func getRealitiveHeightWith(value: CGFloat) -> CGFloat {
        //__________
        var rightHeight: CGFloat
        let availableHeight: CGFloat = 120
        
        guard let maxValue: CGFloat = bpms.map({ $0.value }).max()
        else { return 0.0 }
        
        rightHeight = (value / maxValue) * availableHeight
        return rightHeight
    }
    /*...................................................*/
    
}// END: [STRUCT]

