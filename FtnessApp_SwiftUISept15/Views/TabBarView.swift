import SwiftUI

struct TabBarView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {
            
            TabWaveShape()
                .fill(ColorConstants.topBackground)
                .frame(height: 100)
                .shadow(color: Color.black, radius: 7, x: 10, y: 12)

            /*...................................................*/
            Circle()
                .fill(ColorConstants.hexagonLinear)
                .frame(width: 200, height: 200)
                .offset(x: -155, y: 40)
                .clipShape(
                    Rectangle()
                        .rotation(Angle.degrees(8))
                        .offset(x: -155, y: 55)
                )
                .opacity(0.3)
                .shadow(color: Color.white.opacity(0.55), radius: 7, x: 10, y: 12)

            Circle()
                .fill(ColorConstants.hexagonLinearInverted)
                .frame(width: 200, height: 200)
                .offset(x: 155, y: 40)
                .clipShape(
                    Rectangle()
                        .rotation(Angle.degrees(-8))
                        .offset(x: 155, y: 55)
                )
                .opacity(0.3)
                .shadow(color: Color.white.opacity(0.55), radius: 7, x: -10, y: 12)

            /*...................................................*/
            
            HStack {
                //__________
                // MARK: -∂ Eyedropper
                Image(systemName: "eyedropper.halffull")
                Spacer()// Horizontally spaced
                
                // MARK: -∂ Books
                Image(systemName: "books.vertical.fill")
                Spacer()// Horizontally spaced
                
                // MARK: -∂ Applelogo
                Image(systemName: "applelogo")
            }///:|_HStack_|
            .font(.title)
            .foregroundColor(Color.white)
            .padding(.leading, 30)
            .padding(.trailing, 30)
            /*©---------------------------------©*/
            
        }//||END__PARENT-VSTACK||
        .shadow(color: Color.black, radius: 7, x: 10, y: 12)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]
