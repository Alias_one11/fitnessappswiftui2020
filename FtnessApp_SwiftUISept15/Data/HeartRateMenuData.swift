import SwiftUI

let heartRateMenu: [FitnessAppModel.HeartRateMenu] = [
    //__________
    FitnessAppModel.HeartRateMenu(id: 0, name: "Resting", currentValue: 80),
    FitnessAppModel.HeartRateMenu(id: 1, name: "Gym", currentValue: 140)
]
