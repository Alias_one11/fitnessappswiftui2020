import SwiftUI

let bpms: [FitnessAppModel.BPM] = [
    //__________
    FitnessAppModel.BPM(value: 86, time: "2 pm"),
    FitnessAppModel.BPM(value: 91, time: "3 pm"),
    FitnessAppModel.BPM(value: 84, time: "4 pm"),
    FitnessAppModel.BPM(value: 124, time: "5 pm"),
    FitnessAppModel.BPM(value: 148, time: "6 pm"),
    FitnessAppModel.BPM(value: 116, time: "7 pm")
]
