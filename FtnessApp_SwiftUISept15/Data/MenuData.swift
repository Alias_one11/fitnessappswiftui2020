import SwiftUI

let menus: [FitnessAppModel.DashboardMenu] = [
    //__________
    FitnessAppModel.DashboardMenu(
        title: "Heart Rate ᵇᵖᵐ",
        name: "bolt.heart.fill",
        unit: "bpm",
        currentValue: "120ᵇᵖᵐ"
    ),
    FitnessAppModel.DashboardMenu(
        title: "Disatance \u{2098}",
        name: "bolt",
        unit: "meter",
        currentValue: "290\u{2098}"
    ),
    FitnessAppModel.DashboardMenu(
        title: "Water Intake \u{2098}\u{2098}",
        name: "drop",
        unit: "millimeter",
        currentValue: "480\u{2098}\u{2098}"
    ),
    FitnessAppModel.DashboardMenu(
        title: "Temparature \u{00B0}",
        name: "thermometer",
        unit: "degree",
        currentValue: "98.7\u{00B0}"
    )
]
